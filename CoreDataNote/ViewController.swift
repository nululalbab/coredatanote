//
//  ViewController.swift
//  CoreDataNote
//
//  Created by Najibullah Ulul Albab on 09/05/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    var people: [NSManagedObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      //1 Creating appdelegate
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
          return
      }
      
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      //2 fetching request on entity named Person
      let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "Person")
      
      //3 Fetch to people
      do {
        people = try managedContext.fetch(fetchRequest)
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
      }
    }


    @IBAction func addName(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "New Name",
                                       message: "Add a new name",
                                       preferredStyle: .alert)
         
         let saveAction = UIAlertAction(title: "Save", style: .default) {
           [unowned self] action in
           
           guard let textField = alert.textFields?.first,
             let nameToSave = textField.text else {
               return
           }
           
           self.save(name: nameToSave)
           self.tableView.reloadData()
         }
         
         let cancelAction = UIAlertAction(title: "Cancel",
                                          style: .cancel)
         
         alert.addTextField()
         
         alert.addAction(saveAction)
         alert.addAction(cancelAction)
         
         present(alert, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let person = people[indexPath.row]
         let cell =
           tableView.dequeueReusableCell(withIdentifier: "Cell",
                                         for: indexPath)
         cell.textLabel?.text =
           person.value(forKeyPath: "name") as? String
         return cell
    }
    
    func save(name: String) {
      
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      
      // 1 Get managedContext
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      // 2 Ambil entitas person
      let entity =
        NSEntityDescription.entity(forEntityName: "Person",
                                   in: managedContext)!
      
      let person = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
      
      // 3 Update managedobject person dengan atribut name
      person.setValue(name, forKeyPath: "name")
      
      // 4 save managedObject dan tambah isi array people
      do {
        try managedContext.save()
        people.append(person)
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
             //Refer NSPersistentContainer for AppDelegate
             guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
             
             //We need to create a context from this container
             let managedContext = appDelegate.persistentContainer.viewContext
            
            managedContext.delete(people[indexPath.row] as NSManagedObject)
            people.remove(at: indexPath.row)
            do {
                try managedContext.save()
            }
            catch{
                print(error)
            }
            print("delete")
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Update list",
                                      message: "Update Current list",
                                      preferredStyle: .alert)
        
        let updateAction = UIAlertAction(title: "Update", style: .default) {
          [unowned self] action in
          
          guard let textField = alert.textFields?.first,
            let nameToSave = textField.text else {
              return
          }
            let oldName = self.people[indexPath.row]
            self.update(oldName: oldName,name: nameToSave)
          self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField()
        
        alert.addAction(updateAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
        
        
        
        
    }
    
    func update(oldName:NSManagedObject, name: String) {
      
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      
      // 1 Ambil managed context dari appdelegate
      let managedContext =
        appDelegate.persistentContainer.viewContext
    //2 Ambil ManagedObject khusus ObjectID lama
            let object = managedContext.object(with: oldName.objectID)
            object.setValue("\(name)", forKey: "name")
            do {
                try managedContext.save()
            }
            catch{
                print(error)
            }
        
        
    }
    
    
    
}

